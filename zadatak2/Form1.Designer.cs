﻿namespace WindowsFormsApp3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_zbrajanje = new System.Windows.Forms.Button();
            this.btn_oduzimanje = new System.Windows.Forms.Button();
            this.btn_mnozenje = new System.Windows.Forms.Button();
            this.btn_dijeljenje = new System.Windows.Forms.Button();
            this.btn_sqrt = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.text1 = new System.Windows.Forms.TextBox();
            this.label1_x = new System.Windows.Forms.Label();
            this.btn_pow = new System.Windows.Forms.Button();
            this.btn_sin = new System.Windows.Forms.Button();
            this.btn_cos = new System.Windows.Forms.Button();
            this.btn_log = new System.Windows.Forms.Button();
            this.text2 = new System.Windows.Forms.TextBox();
            this.label2_y = new System.Windows.Forms.Label();
            this.izracunaj = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_zbrajanje
            // 
            this.btn_zbrajanje.Location = new System.Drawing.Point(23, 90);
            this.btn_zbrajanje.Name = "btn_zbrajanje";
            this.btn_zbrajanje.Size = new System.Drawing.Size(75, 23);
            this.btn_zbrajanje.TabIndex = 0;
            this.btn_zbrajanje.Text = "+";
            this.btn_zbrajanje.UseVisualStyleBackColor = true;
            this.btn_zbrajanje.Click += new System.EventHandler(this.btn_zbrajanje_Click);
            // 
            // btn_oduzimanje
            // 
            this.btn_oduzimanje.Location = new System.Drawing.Point(23, 132);
            this.btn_oduzimanje.Name = "btn_oduzimanje";
            this.btn_oduzimanje.Size = new System.Drawing.Size(75, 23);
            this.btn_oduzimanje.TabIndex = 1;
            this.btn_oduzimanje.Text = "-";
            this.btn_oduzimanje.UseVisualStyleBackColor = true;
            this.btn_oduzimanje.Click += new System.EventHandler(this.btn_oduzimanje_Click);
            // 
            // btn_mnozenje
            // 
            this.btn_mnozenje.Location = new System.Drawing.Point(23, 173);
            this.btn_mnozenje.Name = "btn_mnozenje";
            this.btn_mnozenje.Size = new System.Drawing.Size(75, 23);
            this.btn_mnozenje.TabIndex = 2;
            this.btn_mnozenje.Text = "*";
            this.btn_mnozenje.UseVisualStyleBackColor = true;
            this.btn_mnozenje.Click += new System.EventHandler(this.btn_mnozenje_Click);
            // 
            // btn_dijeljenje
            // 
            this.btn_dijeljenje.Location = new System.Drawing.Point(23, 211);
            this.btn_dijeljenje.Name = "btn_dijeljenje";
            this.btn_dijeljenje.Size = new System.Drawing.Size(75, 23);
            this.btn_dijeljenje.TabIndex = 3;
            this.btn_dijeljenje.Text = "/";
            this.btn_dijeljenje.UseVisualStyleBackColor = true;
            this.btn_dijeljenje.Click += new System.EventHandler(this.btn_dijeljenje_Click);
            // 
            // btn_sqrt
            // 
            this.btn_sqrt.Location = new System.Drawing.Point(120, 90);
            this.btn_sqrt.Name = "btn_sqrt";
            this.btn_sqrt.Size = new System.Drawing.Size(75, 23);
            this.btn_sqrt.TabIndex = 4;
            this.btn_sqrt.Text = "√";
            this.btn_sqrt.UseVisualStyleBackColor = true;
            this.btn_sqrt.Click += new System.EventHandler(this.btn_sqrt_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Location = new System.Drawing.Point(227, 239);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(75, 23);
            this.btn_exit.TabIndex = 5;
            this.btn_exit.Text = "Izlaz";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.button6_Click);
            // 
            // text1
            // 
            this.text1.Location = new System.Drawing.Point(23, 49);
            this.text1.Name = "text1";
            this.text1.Size = new System.Drawing.Size(75, 20);
            this.text1.TabIndex = 6;
            // 
            // label1_x
            // 
            this.label1_x.AutoSize = true;
            this.label1_x.Location = new System.Drawing.Point(20, 33);
            this.label1_x.Name = "label1_x";
            this.label1_x.Size = new System.Drawing.Size(12, 13);
            this.label1_x.TabIndex = 7;
            this.label1_x.Text = "x";
            // 
            // btn_pow
            // 
            this.btn_pow.Location = new System.Drawing.Point(120, 132);
            this.btn_pow.Name = "btn_pow";
            this.btn_pow.Size = new System.Drawing.Size(75, 23);
            this.btn_pow.TabIndex = 8;
            this.btn_pow.Text = "x^2";
            this.btn_pow.UseVisualStyleBackColor = true;
            this.btn_pow.Click += new System.EventHandler(this.btn_pow_Click);
            // 
            // btn_sin
            // 
            this.btn_sin.Location = new System.Drawing.Point(120, 173);
            this.btn_sin.Name = "btn_sin";
            this.btn_sin.Size = new System.Drawing.Size(75, 23);
            this.btn_sin.TabIndex = 9;
            this.btn_sin.Text = "sin";
            this.btn_sin.UseVisualStyleBackColor = true;
            this.btn_sin.Click += new System.EventHandler(this.btn_sin_Click);
            // 
            // btn_cos
            // 
            this.btn_cos.Location = new System.Drawing.Point(120, 211);
            this.btn_cos.Name = "btn_cos";
            this.btn_cos.Size = new System.Drawing.Size(75, 23);
            this.btn_cos.TabIndex = 10;
            this.btn_cos.Text = "cos";
            this.btn_cos.UseVisualStyleBackColor = true;
            this.btn_cos.Click += new System.EventHandler(this.btn_cos_Click);
            // 
            // btn_log
            // 
            this.btn_log.Location = new System.Drawing.Point(218, 90);
            this.btn_log.Name = "btn_log";
            this.btn_log.Size = new System.Drawing.Size(75, 23);
            this.btn_log.TabIndex = 11;
            this.btn_log.Text = "log";
            this.btn_log.UseVisualStyleBackColor = true;
            this.btn_log.Click += new System.EventHandler(this.btn_log_Click);
            // 
            // text2
            // 
            this.text2.Location = new System.Drawing.Point(120, 49);
            this.text2.Name = "text2";
            this.text2.Size = new System.Drawing.Size(75, 20);
            this.text2.TabIndex = 12;
            // 
            // label2_y
            // 
            this.label2_y.AutoSize = true;
            this.label2_y.Location = new System.Drawing.Point(117, 33);
            this.label2_y.Name = "label2_y";
            this.label2_y.Size = new System.Drawing.Size(12, 13);
            this.label2_y.TabIndex = 13;
            this.label2_y.Text = "y";
            // 
            // izracunaj
            // 
            this.izracunaj.AutoSize = true;
            this.izracunaj.Location = new System.Drawing.Point(215, 49);
            this.izracunaj.Name = "izracunaj";
            this.izracunaj.Size = new System.Drawing.Size(46, 13);
            this.izracunaj.TabIndex = 14;
            this.izracunaj.Text = "Rezultat";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 274);
            this.Controls.Add(this.izracunaj);
            this.Controls.Add(this.label2_y);
            this.Controls.Add(this.text2);
            this.Controls.Add(this.btn_log);
            this.Controls.Add(this.btn_cos);
            this.Controls.Add(this.btn_sin);
            this.Controls.Add(this.btn_pow);
            this.Controls.Add(this.label1_x);
            this.Controls.Add(this.text1);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_sqrt);
            this.Controls.Add(this.btn_dijeljenje);
            this.Controls.Add(this.btn_mnozenje);
            this.Controls.Add(this.btn_oduzimanje);
            this.Controls.Add(this.btn_zbrajanje);
            this.Name = "Form1";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_zbrajanje;
        private System.Windows.Forms.Button btn_oduzimanje;
        private System.Windows.Forms.Button btn_mnozenje;
        private System.Windows.Forms.Button btn_dijeljenje;
        private System.Windows.Forms.Button btn_sqrt;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.TextBox text1;
        private System.Windows.Forms.Label label1_x;
        private System.Windows.Forms.Button btn_pow;
        private System.Windows.Forms.Button btn_sin;
        private System.Windows.Forms.Button btn_cos;
        private System.Windows.Forms.Button btn_log;
        private System.Windows.Forms.TextBox text2;
        private System.Windows.Forms.Label label2_y;
        private System.Windows.Forms.Label izracunaj;
    }
}

