﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        double x, y;
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_oduzimanje_Click(object sender, EventArgs e)
        {
            double.TryParse(text1.Text, out x);
            double.TryParse(text2.Text, out y);
            if (!double.TryParse(text1.Text, out x))
                MessageBox.Show("Pogresan unos x!", "Pogreska!");
            else if (!double.TryParse(text2.Text, out y))
                MessageBox.Show("Pogresan unos y!", "Pogreska!");
            else
            {
                izracunaj.Text = (x - y).ToString();
            }
        }

        private void btn_mnozenje_Click(object sender, EventArgs e)
        {
            double.TryParse(text1.Text, out x);
            double.TryParse(text2.Text, out y);
            if (!double.TryParse(text1.Text, out x))
                MessageBox.Show("Pogresan unos x!", "Pogreska!");
            else if (!double.TryParse(text2.Text, out y))
                MessageBox.Show("Pogresan unos y!", "Pogreska!");
            else
            {
                izracunaj.Text = (x * y).ToString();
            }
        }

        private void btn_dijeljenje_Click(object sender, EventArgs e)
        {
            double.TryParse(text1.Text, out x);
            double.TryParse(text2.Text, out y);
            if (!double.TryParse(text1.Text, out x))
                MessageBox.Show("Pogresan unos x!", "Pogreska!");
            else if (!double.TryParse(text2.Text, out y))
                MessageBox.Show("Pogresan unos y!", "Pogreska!");
            else if (y == 0)
                MessageBox.Show("Nazivnik ne smije biti 0!");
            else
            {
                 izracunaj.Text = (x / y).ToString();
            }
        }

        private void btn_sqrt_Click(object sender, EventArgs e)
        {
            double.TryParse(Text, out x);
            if (!double.TryParse(text1.Text, out x))
                MessageBox.Show("Pogresan unos x!", "Pogreska!");
            else if (x < 0) MessageBox.Show("Vrijednost pod korijenom ne smije biti negativna!");
            else
            {
                text2.Clear();
                izracunaj.Text = (Math.Sqrt(x)).ToString();
            }
        }

        private void btn_pow_Click(object sender, EventArgs e)
        {
            double.TryParse(text1.Text, out x);
            if (!double.TryParse(text1.Text, out x))
                MessageBox.Show("Pogresan unos x!", "Pogreska!");
            else
            {
                text2.Clear();
                izracunaj.Text = (x * x).ToString();
            }
        }

        private void btn_sin_Click(object sender, EventArgs e)
        {
            double.TryParse(text1.Text, out x);
            if (!double.TryParse(text1.Text, out x))
                MessageBox.Show("Pogresan unos x!", "Pogreska!");
            else
            {
                text2.Clear();
                izracunaj.Text = (Math.Sin(x * Math.PI / 180)).ToString();
            }
        }

        private void btn_cos_Click(object sender, EventArgs e)
        {
            double.TryParse(text1.Text, out x);
            if (!double.TryParse(text1.Text, out x))
                MessageBox.Show("Pogresan unos x!", "Pogreska!");
            else
            {
                text2.Clear();
                izracunaj.Text = (Math.Cos(x * Math.PI / 180)).ToString();
            }
        }

        private void btn_log_Click(object sender, EventArgs e)
        {
            double.TryParse(text1.Text, out x);
            if (!double.TryParse(text1.Text, out x))

                MessageBox.Show("Pogresan unos xc!", "Pogreska!");
            else if (x <= 0)
                MessageBox.Show("x mora biti veci od 0!");
            else
            {
                text2.Clear();
                izracunaj.Text = (Math.Log10(x)).ToString();
            }
         }

        private void button6_Click(object sender, EventArgs e) //izlaz, iako je promijenjno u btn_izlaz ovdje je naziv button6
        {
            Application.Exit();
        }

        private void btn_zbrajanje_Click(object sender, EventArgs e)
        {
            double.TryParse(text1.Text, out x);
            double.TryParse(text2.Text, out y);
            if (!double.TryParse(text1.Text, out x))
                MessageBox.Show("Pogresan unos x!", "Pogreska!");
            else if (!double.TryParse(text2.Text, out y))
                MessageBox.Show("Pogresan unos y!", "Pogreska!");
            else
            {
                izracunaj.Text = (x + y).ToString();
            }
        }
    }
}
