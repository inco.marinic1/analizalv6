﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        List<string> list = new List<string>();
        List<string> pokusaji = new List<string>();
        List<char> slova = new List<char>();
        List<Label> labeli = new List<Label>();
        int broj_pokusaja = 6, brojac = 0, ispravno = 0; 
        string rijec;
        string path = "D:\\rijeci.txt";

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void provjeri_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(text1.Text))
            {
                if (broj_pokusaja == 0 && !rijec.Contains(text1.Text))
                {
                    MessageBox.Show("Iskoristili ste sve pokušaje, igra je gotova", "Game over!");
                    text1.Clear();
                    gameInit();
                }

                else if (rijec.Contains(text1.Text) && !slova.Contains(text1.Text[0]))
                {
                    slova.Add(text1.Text[0]);
                    for (int i = 0; i < rijec.Length; i++)
                    {
                        if (rijec[i] == text1.Text[0])
                        {
                            labeli[i].Text = text1.Text[0].ToString();
                            ispravno++;
                        }
                    }

                    if (ispravno == rijec.Length)
                    {
                        MessageBox.Show("Pogodili ste rijec", "Cestitamo!");
                        gameInit();
                    }
                }
                else if (!slova.Contains(text1.Text[0]))
                {
                    slova.Add(text1.Text[0]);
                    broj_pokusaja--;
                    label2.Text = "Preostali broj pokusaja: " + broj_pokusaja.ToString();
                }
            }
            else
            {
                MessageBox.Show("Neispravan unos", "Greska!");
            }

            text1.Clear();

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            labeli.Add(label4);
            labeli.Add(label5);
            labeli.Add(label6);
            labeli.Add(label7);
            labeli.Add(label7);
            labeli.Add(label9);
            labeli.Add(label10);
            labeli.Add(label11);
            labeli.Add(label12);
            labeli.Add(label13);

            gameInit();


        }

        private void gameInit()
        {
            for (int i = 0; i < 10; i++)
            {
                labeli[i].Text = "_";
                labeli[i].Show();
            }

            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(line);
                    brojac++;
                }
            }

            Random rnd = new Random();
            int tmp = rnd.Next(0, brojac);
            rijec = list[tmp];

            for (int i = 9; i >= rijec.Length; i--)
            {
                labeli[i].Hide();
            }

            ispravno = 0;
            broj_pokusaja = 6;
            slova.Clear();
            label2.Text = "Preostali broj pokusaja: " + broj_pokusaja.ToString();
        }
    }


}
    

 